# vagrant-ansible-packer-elk

This is test environment to use it for testing a migration Typo3. And make a AWS ami build.

**Pre-built Vagrant Box**

Test environment for Typo3 on Debian, Ubuntu or CentOS.

This example build configuration installs and configures Debian 10 amd64 minimal using Ansible, and then generates a Vagrant box file for VirtualBox.

The example can be modified to use more Ansible roles, plays, and included playbooks to fully configure (or partially) configure a box file suitable for deployment for development environments.

You can use Amazon ami builder too.

## Requirements

The following software must be installed/present on your local machine before you can use Packer to build the Vagrant box file:

 * [Packer](http://www.packer.io/)
 * [Vagrant](http://vagrantup.com/)
 * [VirtualBox](https://www.virtualbox.org/) (if you want to build the VirtualBox box)
 * [Ansible](http://docs.ansible.com/intro_installation.html)

### AWS Environment

For you AWS invironement it is importand what you use vars in you terminal or coustomized the Vars section but is not secure !

Example for vars in you terminal are working on Linux Mac OSX and Windows 10 Linux Subseytem.

Edit your _.bashrc_ and add the following at the and auf the file.

```s
if [ -x "~/.aws-env" ]
  then
  . ~/.aws-env"
fi
```

In _~/.aws_env_ you have too add own vars. For example

```s
region = "eu-central-1"
aws_access_key_id = "xxxxxxxxxxx"
aws_secret_access_key = "8+xxxxxxxxxxx1"
```

## Usage

Make sure all the required software (listed above) is installed, then cd to the directory containing this README.md file, and run:

```s
packer build -var 'distribution=ubuntu' -var '' -var '' ubuntu-2004.json
```

alternativ you can use the _packer-vars_ file too set 

After a few minutes, Packer should tell you the box was generated successfully, and the box can uploaded to Vagrant Cloud.

> **Note**: This configuration includes a post-processor that pushes the built box to Vagrant Cloud (which requires a `VAGRANT_CLOUD_TOKEN` environment variable to be set) by default it is commented out; decommented the `vagrant-cloud` post-processor from the Packer template to build the box locally and not push it to Vagrant Cloud is the default.
You don't need to specify a `version` variable either, if not using the `vagrant-cloud` post-processor.

```json
  "post-processors": [
    [
      {
        "output": "builds/{{.Provider}}--debian-10.box",
        "type": "vagrant"
      },
      {
        "type": "vagrant-cloud",
        "box_tag": "tobkern1980/debian10",
        "version": "{{user `version`}}"
      }
    ]
  ]
```

## Testing built boxes

There's an included Vagrantfile that allows quick testing of the built Vagrant boxes. From this same directory, run the following command after building the box:

```sh
vagrant up
```

* [Ubuntu 20.04 LTS Cloud Images](https://cloud-images.ubuntu.com/focal/current/)
* [AWS EC2 AMI Locator](http://cloud-images.ubuntu.com/locator/ec2/?_ga=2.262160332.139064778.1581676436-906141763.1578405198)
* [EC2StartersGuide](https://help.ubuntu.com/community/EC2StartersGuide?_ga=2.262160332.139064778.1581676436-906141763.1578405198)

SHA256
```
15d367c723a0d280033df04138ff49e7de4b7a77 *focal-server-cloudimg-amd64-azure.vhd.tar.gz
7ab3b9b53c15adfc5fbca3119f84272ea040e0ea *focal-server-cloudimg-amd64-disk-kvm.img
8a293803d0c1cbf7958e1b2c7d715a6b810c52a4 *focal-server-cloudimg-amd64-lxd.tar.xz
2f0e56066ed4c635763e5da50a7afa1d2cc0625b *focal-server-cloudimg-amd64-root.tar.xz
b15e73c898e3f005f0093d4b23c5598e902e7132 *focal-server-cloudimg-amd64-vagrant.box
498ecb88407032496c3e8ef55a18ccb24baca67c *focal-server-cloudimg-amd64-wsl.rootfs.tar.gz
24bb98bcfde580caedea8cc0ba35923668dc6656 *focal-server-cloudimg-amd64.img
e0b1bf1f5a1370a569116173d53f64b5198b1b0b *focal-server-cloudimg-amd64.ova
0bf0a244ce2815244d61ba5ba6db9c043d5ee757 *focal-server-cloudimg-amd64.squashfs
a4b803781a59734181277bc7b815535f0bb333ce *focal-server-cloudimg-amd64.tar.gz
349e6f90c7aa60d5b993c6cc828eaf0d48bae868 *focal-server-cloudimg-amd64.vmdk
7cb1eaf44c54dad226392964a5927d065151f6f1 *focal-server-cloudimg-arm64-lxd.tar.xz
0f4eecfa0e7b8570fbb49307e20ee4357f07e8a0 *focal-server-cloudimg-arm64-root.tar.xz
8d786e726371d56161fddbe81656326e25b86add *focal-server-cloudimg-arm64-wsl.rootfs.tar.gz
e89537d9aaa466416e4785615c4b0b8f1d43dc2a *focal-server-cloudimg-arm64.img
819d5f0f057dae2117e8c9687e2d6812095bc8ff *focal-server-cloudimg-arm64.squashfs
3ea2a265ddb86ffa5328853911e5bb463ac04de7 *focal-server-cloudimg-arm64.tar.gz
8f43f984296d882c624045c7c8e930cfff53e7ec *focal-server-cloudimg-armhf-lxd.tar.xz
4f8113b02c0ad200166b4f01a078d92f83bba7cf *focal-server-cloudimg-armhf-root.tar.xz
048fd0708bd1cb0be9016130ba424c3a47b3bfa0 *focal-server-cloudimg-armhf.img
7fb6b35d43be957ffa3b859b4672b7eed799d3ea *focal-server-cloudimg-armhf.squashfs
8652e33350d417980349bc6b7fb8ea021d25693a *focal-server-cloudimg-armhf.tar.gz
b3bac837845562a19375af99a6620f54b80074d5 *focal-server-cloudimg-ppc64el-lxd.tar.xz
0d081c39790e1e430600071f47ffe8322a26626d *focal-server-cloudimg-ppc64el-root.tar.xz
0cdeda11c8225055945982866ec2330590205cf9 *focal-server-cloudimg-ppc64el.img
aeaf7cd24baea54279473bdb215af28d2735175b *focal-server-cloudimg-ppc64el.squashfs
9266639606c786667363dfb2bfd8dfc64d3fe08b *focal-server-cloudimg-ppc64el.tar.gz
766dd218954fed2c62e7be6058d6543d4b87b68d *focal-server-cloudimg-s390x-lxd.tar.xz
e2ef8ba51a5742da0d51d7b04df29271a3861086 *focal-server-cloudimg-s390x-root.tar.xz
6c3a4d171e2dc424a9c19cc6f4b50865e73fa6ba *focal-server-cloudimg-s390x.img
4047231b93681e8db0383dc0911c78129b266d27 *focal-server-cloudimg-s390x.squashfs
84ccd1fec627f34df9fc20132c5b4eac731246a7 *focal-server-cloudimg-s390x.tar.gz

```

### Amazon Permissions

You'll need at least the following permissions in the policy for your IAM user in order to successfully upload an image via the amazon-import post-processor

```json
      "ec2:CancelImportTask",
      "ec2:CopyImage",
      "ec2:CreateTags",
      "ec2:DescribeImages",
      "ec2:DescribeImportImageTasks",
      "ec2:ImportImage",
      "ec2:ModifyImageAttribute",
      "ec2:DeregisterImage"
```
