#!/bin/bash

PACKER_BIN = $(type -p packer)


if [ -x $(pwd)/packer-vars ] 
  then
    . packer-env
fi

if [ ! -x $PACKER_BIN ]
    then
    echo "Packer is not installed"
    echo "Script will be exit"
    exit 1
fi

# Add File with vars
${PACKER_BIN}

