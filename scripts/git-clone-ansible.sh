#!/bin/bash
# 
# 
# for i in clone.git 
#  do
#   clone $i 
#  done
#

PWD_ROLES=$(pwd)
ROLES_DIR=$PWD_ROLES/roles

git clone https://github.com/geerlingguy/ansible-role-apache.git "$ROLES_DIR/ansible-role-apache"
git clone https://github.com/geerlingguy/ansible-role-pip.git "$ROLES_DIR/ansible-role-pip"
git clone https://github.com/geerlingguy/ansible-role-docker.git "$ROLES_DIR/ansible-role-docker"
git clone https://github.com/geerlingguy/ansible-role-composer.git "$ROLES_DIR/ansible-role-composer"
git clone https://github.com/geerlingguy/ansible-role-git.git "$ROLES_DIR/ansible-role-git"
git clone https://github.com/geerlingguy/ansible-role-github-users.git "$ROLES_DIR/ansible-role-github-users"
git clone https://github.com/geerlingguy/ansible-role-nginx.git "$ROLES_DIR/ansible-role-nginx"
git clone git@gitlab.louis-net.de:ansible/ansible-role-louis-mariadb.git "$ROLES_DIR/ansible-role-louis-mariadb"
git clone git@gitlab.louis-net.de:ansible/ansible-role-louis-pro-ftpd.git "$ROLES_DIR/ansible-role-louis-pro-ftpd"
git clone git@gitlab.louis-net.de:ansible/ansible-role-louis-phpmyadmin.git "$ROLES_DIR/ansible-role-louis-phpmyadmin"
git clone git@gitlab.louis-net.de:ansible/ansible-role-louis-filebeat.git "$ROLES_DIR/ansible-role-louis-filebeat"
git clone git@gitlab.louis-net.de:ansible/ansible-role-louis-php.git "$ROLES_DIR/ansible-role-louis-php"
git clone git@gitlab.louis-net.de:ansible/ansible-role-louis-mkhosting.git "$ROLES_DIR/ansible-role-louis-mkhosting"
git clone git@gitlab.louis-net.de:ansible/ansible-role-louis-postfix_slave.git "$ROLES_DIR/ansible-role-louis-postfix-slave"
git clone git@gitlab.louis-net.de:ansible/ansible-role-louis-solr.git "$ROLES_DIR/ansible-role-louis-solr"
git clone https://github.com/holms/ansible-fqdn.git "$ROLES_DIR/ansible-role-fqdn"
git clone https://github.com/geerlingguy/ansible-role-apache.git "$ROLES_DIR/ansible-role-apache"
git clone git@gitlab.louis-net.de:ansible/ansible-role-louis-apache.git "$ROLES_DIR/ansible-role-louis-apache"
git clone https://github.com/robertdebock/ansible-role-users.git "$ROLES_DIR/ansible-role-users"
git clone https://github.com/geerlingguy/ansible-role-certbot.git "$ROLES_DIR/ansible-role-certbot"
git clone https://github.com/geerlingguy/ansible-role-apache-php-fpm.git "$ROLES_DIR/ansible-role-apache-php-fpm"
git clone https://github.com/geerlingguy/ansible-role-php-versions.git "$ROLES_DIR/ansible-role-php-versions"
git clone https://github.com/geerlingguy/ansible-role-php.git "$ROLES_DIR/ansible-role-php"
#git clone https://github.com/adfinis-sygroup/ansible-role-mariadb.git "$ROLES_DIR/ansible-role-mariadb" # nicht aktuell
#git clone https://github.com/tschifftner/ansible-role-mariadb.git "$ROLES_DIR/ansible-role-mariadb" # build faling
#git clone https://github.com/diodonfrost/ansible-role-mariadb.git "$ROLES_DIR/ansible-role-mariadb" # gut bisher
