#!/bin/bash
# Builds Ansible Dashboard

ansible -i test.cfg  -m setup --tree ../ansible/out/ test.srv.louis.info --vault-pass ~/.vault_pass.txt
ansible-cmdb out/ > index.html