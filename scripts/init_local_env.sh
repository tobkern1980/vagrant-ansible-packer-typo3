#!/bin/bash
# https://stackoverflow.com/questions/59838/how-to-check-if-a-directory-exists-in-a-bash-shell-script
# 

if [ ! -d "~/ansible/host_vars" ] ; then
    echo "Make Direcory ~/ansible/host_vars"
    mkdir -p "~/ansible/host_vars"
else 
    echo "Directory ~/ansible/host_vars already exists"
fi

if [ ! -e "~/ansible/host_vars/$(hostname)" ] ; then
    echo "Add $(hostname) to ~/ansible/host_vars/$(hostname)"
    touch ~/ansible/host_vars/$(hostname)
else
    echo "~/ansible/host_vars/$(hostname) already exists"
fi

if [ ! -s "~/.env_ansible" ]; then
    echo "Copy env_ansible to ~/.env_ansible"
    cp $(pwd)/scripts/env_ansible  "~/.env_ansible"
else
    echo " ~/.env_ansible already exists" 
fi

if [ ! -s "~/ansible/$(hostname)" ]; then
    echo " Set Inventory ENV to ~/ansible/$(hostname)"
    #sed -i  //g 
else
    echo " ~/.env_ansible already exists" 
fi

# # grep ".env_ansible" ~/.bashrc