#!/bin/bash
# 
# 
# for i in pull.git 
#  do
#   pull $i 
#  done
#

GET_CHANGED=$(git pull --dry-run | grep -q -v 'Already up-to-date.' && CHANGED="1" && export $CHANGED)

PWD_ROLES=$(pwd)
ROLES_DIR=$PWD_ROLES/roles

DIRS=( "$ROLES_DIR/ansible-role-apache"
  "$ROLES_DIR/ansible-role-pip"
  "$ROLES_DIR/ansible-role-docker"
  "$ROLES_DIR/ansible-role-composer"
  "$ROLES_DIR/ansible-role-git"
  "$ROLES_DIR/ansible-role-github-users"
  "$ROLES_DIR/ansible-role-nginx"
  "$ROLES_DIR/ansible-role-louis-mariadb"
  "$ROLES_DIR/ansible-role-louis-pro-ftpd"
  "$ROLES_DIR/ansible-role-louis-phpmyadmin"
  "$ROLES_DIR/ansible-role-louis-filebeat"
  "$ROLES_DIR/ansible-role-louis-php"
  "$ROLES_DIR/ansible-role-louis-apache"
  "$ROLES_DIR/ansible-role-louis-mkhosting"
  "$ROLES_DIR/ansible-role-louis-postfix-slave"
  "$ROLES_DIR/ansible-role-louis-solr"
  "$ROLES_DIR/ansible-role-fqdn"
  "$ROLES_DIR/ansible-role-users"
  "$ROLES_DIR/ansible-role-php"
  "$ROLES_DIR/ansible-role-certbot"
  "$ROLES_DIR/ansible-role-apache-php-fpm"
  "$ROLES_DIR/ansible-role-php-versions"
  #"$ROLES_DIR/ansible-role-mariadb"
)

for dir in "${DIRS[@]}"
    do
        #GET_CHANGED=$(git pull --dry-run | grep -q -v 'Already up-to-date.' && CHANGED="1" && export $CHANGED)
        #if [ $# -ne 0 ] ; then
        echo "Pull $dir"
            git pull $dir
        #else echo Dir $dir: Not CHANGED
        #fi
done